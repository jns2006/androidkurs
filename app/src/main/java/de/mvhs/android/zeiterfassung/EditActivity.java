package de.mvhs.android.zeiterfassung;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;

import de.mvhs.android.zeiterfassung.db.TimeDataContract;
import de.mvhs.android.zeiterfassung.db.TimeDataProvider;

/**
 * Created by eugen on 06.11.16.
 */

public class EditActivity extends AppCompatActivity {
  public final static String ID_KEY = "EditItemId";
  public final static long NEW_ID = -1;
  private long _id = NEW_ID;

  //Controls
  private EditText _startDate;
  private EditText _startTime;
  private EditText _endDate;
  private EditText _endTime;
  private EditText _pause;
  private EditText _comment;
  private Button _saveButton;

  //Werte der Controls
  private Calendar _endDateTimeValue;
  private Calendar _startDateTimeValue;
  private String _commentValue;
  private int _pauseValue;

  //DateTime format für Datum und Uhrzeit
  private DateFormat _dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT);
  private DateFormat _timeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT);

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_grid);

    // Auslesen der ID aus Metainformationen
    _id = getIntent().getLongExtra(ID_KEY, NEW_ID);

    // Initialisieren der UI Elemente
    _startDate = (EditText) findViewById(R.id.StartDateValue);
    _startDate.setKeyListener(null);

    _startTime = (EditText) findViewById(R.id.StartTimeValue);
    _startTime.setKeyListener(null);

    _endDate = (EditText) findViewById(R.id.EndDateValue);
    _endDate.setKeyListener(null);

    _endTime = (EditText) findViewById(R.id.EndTimeValue);
    _endTime.setKeyListener(null);

    _saveButton = (Button) findViewById(R.id.SaveChangesButton);
    _comment = (EditText) findViewById(R.id.CommentValue);
    _pause = (EditText) findViewById(R.id.PauseValue);

    //OnClickListener für Save Button
    _saveButton.setOnClickListener(new OnClickListenerSaveButton());
  }

  //OnClickListener für SaveButton implementation
  class OnClickListenerSaveButton implements View.OnClickListener {

    @Override
    public void onClick(View v) {
      //Updating an existing record
      if(_id != NEW_ID) {
        updateTimeRecord();

        //Show information for user
        Toast.makeText(EditActivity.this,
                R.string.RecordUpdatedMessage,
                Toast.LENGTH_SHORT).show();
      }
      //Creating a new record
      else{
        createNewTimeRecord();

        //Show information for user
        Toast.makeText(EditActivity.this,
                R.string.RecordCreatedMessage,
                Toast.LENGTH_SHORT).show();
      }
    }

    private void updateTimeRecord(){
      ContentValues values = new ContentValues();

      values.put(TimeDataContract.TimeData.Columns.COMMENT, getCommentValue());
      values.put(TimeDataContract.TimeData.Columns.PAUSE, getPauseValue());

      Uri updateUri = ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _id);
      getContentResolver().update(updateUri,
              values,
              null,
              null);
    }


    private void createNewTimeRecord(){
      ContentValues values = new ContentValues();

      values.put(TimeDataContract.TimeData.Columns.COMMENT, getCommentValue());
      values.put(TimeDataContract.TimeData.Columns.PAUSE, getPauseValue());
      values.put(TimeDataContract.TimeData.Columns.START, getStartValue());
      values.put(TimeDataContract.TimeData.Columns.END, getEndValue());

      getContentResolver().insert(TimeDataContract.TimeData.CONTENT_URI, values);
    }
  }

  private String getStartValue() {
    return TimeDataContract.Converter.formatForDb(_startDateTimeValue);
  }

  private String getEndValue() {
    return TimeDataContract.Converter.formatForDb(_endDateTimeValue);
  }

  @Override
  protected void onStart() {
    super.onStart();

    LoadData();
  }

  private void LoadData(){

    // Neuer Datensatz
    if (_id == NEW_ID){
      setDataForNewRecord();
      return;
    }

    // Vorhandener Datensatz
    Uri dataUri =
            ContentUris.withAppendedId(TimeDataContract.TimeData.CONTENT_URI, _id);

    Cursor data = getContentResolver().query(dataUri, null, null, null, null);

    // Daten lesen
    if (data.moveToFirst()){

      setDataForExistingRecord(data);
    }

    data.close();
  }

  private void setDataForExistingRecord(Cursor data) {
    //Startzeit aus der DB auslesen
    String startDateTimeFromDb = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.START));

    //Endzeit aus der DB auslesen
    String endDateTimeFromDb = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.END));

    _startDateTimeValue = getConvertedDateTimeFromDb(startDateTimeFromDb);
    _endDateTimeValue = getConvertedDateTimeFromDb(endDateTimeFromDb);

    //Comment aus der DB
    _commentValue = data.getString(data.getColumnIndex(TimeDataContract.TimeData.Columns.COMMENT));

    //Pause aus der DB
    _pauseValue = data.getInt(data.getColumnIndex(TimeDataContract.TimeData.Columns.PAUSE));

    //die Werte auf der Oberfläche setzen
   setDataOnUi();
  }

  private void setDataForNewRecord() {

    //Setting default values ofr a new record
    _startDateTimeValue = Calendar.getInstance();
    _endDateTimeValue = Calendar.getInstance();

    //die Werte auf der Oberfläche setzen
    setDataOnUi();
  }

  private void setDataOnUi() {
    if(_startDateTimeValue != null) {
      _startDate.setText(_dateFormatter.format(_startDateTimeValue.getTime()));
      _startTime.setText(_timeFormatter.format(_startDateTimeValue.getTime()));
    }

    if(_endDateTimeValue != null) {
      _endDate.setText(_dateFormatter.format(_endDateTimeValue.getTime()));
      _endTime.setText(_timeFormatter.format(_endDateTimeValue.getTime()));
    }

    _comment.setText(_commentValue);
    _pause.setText(Integer.toString(_pauseValue));
  }

  private Calendar getConvertedDateTimeFromDb(String valueFromDb) {
    try {
      return TimeDataContract.Converter.parseFromDb(valueFromDb);
    }catch(ParseException e)
    {
      e.printStackTrace();
      return null;
    }
    catch(NullPointerException e)
    {
      e.printStackTrace();
      return null;
    }
  }

  private String getCommentValue()
  {
    return _comment.getText().toString();
  }

  private int getPauseValue() {
    try
    {
      return Integer.parseInt(_pause.getText().toString());
    }
    catch (NumberFormatException e)
    {
      e.printStackTrace();
      return 0;
    }
  }

}
